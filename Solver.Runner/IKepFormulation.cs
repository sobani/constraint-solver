using Gurobi;

namespace Solver.Runner;

public interface IKepFormulation
{
    Result Run(GRBEnv env, bool[,] A, double[,] w);
}

public record Result(double Objective, TimeSpan SetupTime, TimeSpan RunningTime, double ObjectiveGap);

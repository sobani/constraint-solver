using Gurobi;
using Solver.Lib;

namespace Solver.Runner;

public static class ModelHelper
{
    public static (bool[,] A, double[,] w) CreateCompatibility(int n, double density, bool realWeights, int seed)
    {
        var A = new bool[n, n];
        var w = new double[n, n];
        var rng = new Random(seed);
        
        // create the array by start top left and then expand to the bottom right by adding 'rings' / L-shapes
        // that way you ensure a larger size always improves the objective
        for (int i = 0; i < n; i++)
        for (int j = 0; j < i; j++)
        {
            // activation and weight are independent
            // should the density be increased, then the weights will not change
            A[i, j] = rng.NextDouble() < density;
            w[i, j] = rng.NextDouble();

            A[j, i] = rng.NextDouble() < density;
            w[j, i] = rng.NextDouble();
        }

        // we generate the weights even if we don't need them to force the RNG to generate those values
        // and thereby keeping the values for the A matrix the same
        if (!realWeights)
            w = ToDouble(A);

        return (A, w);
    }

    public static double[,] ToDouble(bool[,] b)
    {
        var (lengthI, lengthJ) = b.Dim();
        var result = new double[lengthI, lengthJ];
        
        for (int i = 0; i < lengthI; i++)
        for (int j = 0; j < lengthJ; j++)
            result[i, j] = b[i, j] ? 1 : 0;

        return result;
    }

    public static IEnumerable<(int i, int j)> Indices(bool[,] enabled)
    {
        var (lengthI, lengthJ) = enabled.Dim();

        for (int i = 0; i < lengthI; i++)
        for (int j = 0; j < lengthJ; j++)
        {
            if (enabled[i, j]) yield return (i, j);
        }
    }

    public static GRBLinExpr SumSum(double[,] weights, GRBVar?[,] variables)
    {
        var (lengthI, lengthJ) = variables.Dim();

        var sum = new GRBLinExpr();
        for (int i = 0; i < lengthI; i++)
        for (int j = 0; j < lengthJ; j++)
        {
            if (!ReferenceEquals(variables[i, j], null))
                sum.AddTerm(weights[i, j], variables[i, j]);
        }

        return sum;
    }

    public static GRBLinExpr SumSum(double[,] weights, GRBLinExpr?[,] expressions)
    {
        var (lengthI, lengthJ) = expressions.Dim();

        var sum = new GRBLinExpr();
        for (int i = 0; i < lengthI; i++)
        for (int j = 0; j < lengthJ; j++)
        {
            if (!ReferenceEquals(expressions[i, j], null))
                sum.MultAdd(weights[i, j], expressions[i, j]);
        }

        return sum;
    }
}
using System.Diagnostics;
using Gurobi;

namespace Solver.Runner;

public abstract class GurobiFormulation : IKepFormulation
{
    public Result Run(GRBEnv env, bool[,] A, double[,] w)
    {
        var sw = Stopwatch.StartNew();
        
        var problem = CreateModel(env, A, w);
        var setupTime = sw.Elapsed;
        
        problem.Optimize();
        var runningTime = TimeSpan.FromSeconds(problem.Runtime);

        var objective = -problem.ObjVal;
        var gap = problem.MIPGap;
        
        return new Result(objective, setupTime, runningTime, gap);
    }

    public abstract GRBModel CreateModel(GRBEnv env, bool[,] A, double[,] w);
}

﻿using System.Globalization;

namespace Solver.Explorer;

public static class Program
{
    private static readonly List<string> Formulations = [];
    private static readonly List<string> N = [];
    private static readonly List<string> K = [];
    private static readonly List<string> Densities = [];
    private static readonly List<string> W = [];
    private static readonly List<string> Setup = [];
    private static readonly List<string> Run = [];
    private static readonly List<string> Total = [];
    private static readonly List<string> Gap = [];

    private static readonly List<string> SortProps = [];
    private static readonly List<string> Show = [];
    
    public static void Main(string[] args)
    {
        if (args is [])
            args = ["output.dat"];
        
        var lines = args.SelectMany(File.ReadAllLines).ToList();
        var results = lines.Select(Result.Parse).ToList();
        
        Show.Add("count");
        Display(results);
        
        while (true)
        {
            Console.Write("> ");
            var command = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(command))
                continue;
            if ("quit".StartsWith(command))
                return;

            ParseCommand(command);
            var filtered = Filter(results);
            var sorted = Sort(filtered);
            Display(sorted);
        }
    }

    private static void ParseCommand(string command)
    {
        Show.Clear();
        Show.Add("count");
        
        var current = Show;
        var parts = command.Split();
        foreach (var arg in parts)
        {
            switch (arg.ToLower())
            {
                case "f":
                    current = Formulations;
                    break;
                case "n":
                    current = N;
                    break;
                case "k":
                    current = K;
                    break;
                case "d":
                    current = Densities;
                    break;
                case "w":
                    current = W;
                    break;
                case "setup":
                    current = Setup;
                    break;
                case "run":
                    current = Run;
                    break;
                case "t":
                case "total":
                    current = Total;
                    break;
                case "g":
                case "gap":
                    current = Gap;
                    break;
                
                case "sort":
                    current = SortProps;
                    break;
                case "show":
                    current = Show;
                    break;
                case "reset":
                    Formulations.Clear();
                    N.Clear();
                    K.Clear();
                    Densities.Clear();
                    W.Clear();
                    Setup.Clear();
                    Run.Clear();
                    Total.Clear();
                    Gap.Clear();
                    SortProps.Clear();
                    break;
                default:
                    if (arg.StartsWith('+'))
                    {
                        current.AddRange(arg[1..].Split(','));
                    }
                    else if (arg.StartsWith('-'))
                    {
                        foreach (var part in arg[1..].Split(',')) 
                            current.Remove(part);
                    }
                    else if (arg == "*")
                    {
                        current.Clear();
                    }
                    else
                    {
                        current.Clear();
                        current.AddRange(arg.Split(','));
                    }
                    break;
            }
        }
    }

    private static List<Result> Filter(List<Result> allResults)
    {
        var result = new List<Result>(allResults);
        if (Formulations.Count > 0) 
            result.RemoveAll(r => !Formulations.Contains(r.Formulation));
        if (N.Count > 0)
            FilterNumbers(result, N, r => r.N);
        if (Densities.Count > 0)
            FilterNumbers(result, Densities, r => r.D);
        if (K.Count > 0)
            FilterNumbers(result, K, r => r.K);
        if (W.Count > 0)
            FilterNumbers(result, W, r => r.W);
        if (Setup.Count > 0)
            FilterNumbers(result, Setup, r => r.Setup);
        if (Run.Count > 0)
            FilterNumbers(result, Run, r => r.Run);
        if (Total.Count > 0)
            FilterNumbers(result, Total, r => r.TotalTime);
        if (Gap.Count > 0)
            FilterNumbers(result, Gap, r => r.Gap);

        return result;
    }

    private static void FilterNumbers(List<Result> result, List<string> filters, Func<Result, int> propSelector)
    {
        if (filters.All(f => !f.Contains("..")))
        {
            result.RemoveAll(r => !filters.Contains(propSelector(r).ToString()));
            return;
        }

        int min = 0, max = 0;
        foreach (var filter in filters)
        {
            if (filter.StartsWith(".."))
            {
                min = 0;
                max = int.Parse(filter[2..]);
            } else if (filter.EndsWith(".."))
            {
                min = int.Parse(filter[..^2]);
                max = int.MaxValue;
            }
            else
            {
                var parts = filter.Split("..");
                min = int.Parse(parts[0]);
                max = int.Parse(parts[1]);
            }
        }

        result.RemoveAll(r =>
        {
            var prop = propSelector(r);
            return prop < min || max < prop;
        });
    }

    private static void FilterNumbers(List<Result> result, List<string> filters, Func<Result, double> propSelector)
    {
        if (filters.All(f => !f.Contains("..")))
        {
            var values = filters.Select(double.Parse).ToList();
            result.RemoveAll(r => !values.Contains(propSelector(r)));
            return;
        }

        double min = 0, max = 0;
        foreach (var filter in filters)
        {
            if (filter.StartsWith(".."))
            {
                min = 0;
                max = double.Parse(filter[2..]);
            } else if (filter.EndsWith(".."))
            {
                min = double.Parse(filter[..^2]);
                max = double.MaxValue;
            }
            else
            {
                var parts = filter.Split("..");
                min = double.Parse(parts[0]);
                max = double.Parse(parts[1]);
            }
        }

        result.RemoveAll(r =>
        {
            var prop = propSelector(r);
            return prop < min || max < prop;
        });
    }

    private static List<Result> Sort(List<Result> results)
    {
        if (SortProps is [])
            return results;

        var comparison = SortProps.Select(GetComparison)
            .Aggregate(Combine);
        
        var sorted = new List<Result>(results);
        sorted.Sort(comparison);
        return sorted;

        Comparison<T> Combine<T>(Comparison<T> comp1, Comparison<T> comp2)
        {
            return (x, y) =>
            {
                var first = comp1(x, y);
                return first == 0 ? comp2(x, y) : first;
            };
        }
    }

    private static Comparison<Result> GetComparison(string prop)
    {
        return prop switch
        {
            "f" or "formulation" => (r1, r2) => String.CompareOrdinal(r1.Formulation, r2.Formulation),
            "n" => (r1, r2) => r1.N.CompareTo(r2.N),
            "k" => (r1, r2) => r1.K.CompareTo(r2.K),
            "d" => (r1, r2) => r1.D.CompareTo(r2.D),
            "w" => (r1, r2) => r1.W.CompareTo(r2.W),
            "s" or "seed" => (r1, r2) => r1.Seed.CompareTo(r2.Seed),
            "setup" => (r1, r2) => r1.Setup.CompareTo(r2.Setup),
            "run" => (r1, r2) => r1.Run.CompareTo(r2.Run),
            "t" or "total" => (r1, r2) => r1.TotalTime.CompareTo(r2.TotalTime),
            "g" or "gap" => (r1, r2) => r1.Gap.CompareTo(r2.Gap),
            // objective values are sorted in descending order
            "o" or "obj" or "objective" => (r1, r2) => -r1.ObjVal.CompareTo(r2.ObjVal),
            _ => throw new ArgumentOutOfRangeException(nameof(prop), prop, "unknown property: " + prop)
        };
    }

    private static void Display(List<Result> results)
    {
        foreach (var showCommand in Show)
        {
            switch (showCommand.ToLower())
            {
                case "count":
                    Console.WriteLine(results.Count + " results");
                    break;
                case "all":
                    Console.WriteLine(Result.Header);
                    foreach (var result in results)
                        Console.WriteLine(result);
                    break;
                case "mean":
                    var resultMeans = results.GroupByAdjacent(
                        r => (r.Formulation, r.N, r.D, r.K, r.W),
                        (_, group) => Result.Mean(group));

                    Console.WriteLine(ResultMean.Header);
                    foreach (var resultMean in resultMeans)
                        Console.WriteLine(resultMean);
                    break;
                case "summary":
                    var formulations = results.Select(r => r.Formulation).Distinct().Order(StringComparer.OrdinalIgnoreCase);
                    Console.WriteLine("formulations: " + string.Join(", ", formulations));
                    Console.Write("n: " + string.Join(", ", results.Select(r => r.N).Distinct()));
                    Console.Write("    d: " + string.Join(", ", results.Select(r => r.D).Distinct()));
                    Console.Write("    w: " + string.Join(", ", results.Select(r => r.W).Distinct()));
                    Console.WriteLine("    k: " + string.Join(", ", results.Select(r => r.K).Distinct()));
                    Console.WriteLine("time: " + results.Stats(r => r.TotalTime));
                    Console.WriteLine("gap: " + results.Stats(r => r.Gap));
                    break;
            }
        }
    }
    

    public sealed record Result(
        string Formulation,
        int N,
        int K,
        int D,
        int Seed,
        int W,
        int Setup,
        int Run,
        double ObjVal,
        double Gap)
    {
        public const string Header = "formulation        n  k d% sd w? setu run tot.  objective        gap        UB";

        public double UB => ObjVal + ObjVal * Gap;

        public int TotalTime => Math.Min(1800, Setup + Run);

        public static Result Parse(string line)
        {
            var parts = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            
            var formulation = parts[0];
            var n = int.Parse(parts[1]);
            var k = int.Parse(parts[2]);
            var d = int.Parse(parts[3]);
            var a = int.Parse(parts[4]);
            var l = int.Parse(parts[5]);
            var seed = int.Parse(parts[6]);
            var w = int.Parse(parts[7]);
            var setup = int.Parse(parts[8]);
            var run = int.Parse(parts[9]);
            run = Math.Min(1800, run);
            var objective = double.Parse(parts[10]);
            var gap = double.Parse(parts[11]);
            
            return new Result(formulation, n, k, d, seed, w, setup, run, objective, gap);
        }

        public static ResultMean Mean(IReadOnlyCollection<Result> results)
        {
            results = results.ToList();
            var first = results.First();
            return new ResultMean(
                first.Formulation,
                first.N,
                first.K,
                first.D,
                first.W,
                results.Count(),
                results.Stats(r => r.Setup),
                results.Stats(r => r.Run),
                results.Stats(r => r.TotalTime),
                results.Stats(r => r.Gap));
        }

        public override string ToString()
        {
            return $"{Formulation,-15} {N,4} {K,2} {D,2} {Seed,3} {W,1} {Setup,3} {Run,4} {TotalTime,4} {Left(ObjVal, 10)} {Left(Gap, 10)}, {Left(UB, 10)}";
        }

        private static string Left(double number, int length)
        {
            string output = "";
            int decimalPlaces = length - 2; //because every decimal contains at least "0."
            bool isError = true;

            while (isError && decimalPlaces >= 0)
            {
                output = Math.Round(number, decimalPlaces).ToString(NumberFormatInfo.InvariantInfo);
                isError = output.Length > length;
                decimalPlaces--;
            }

            if (isError)
                throw new FormatException($"{number} can't be represented in {length} characters");
        
            return output.PadLeft(length);
        }
    }

    public sealed record ResultMean(
        string Formulation,
        int N,
        int K,
        int D,
        int W,
        int Count,
        Stats Setup,
        Stats Run,
        Stats Total,
        Stats Gap)
    {
        public const string Header = "formulation        n  k d% w? #     setup          run        total           gap    ";

        public override string ToString()
        {
            return $"{Formulation,-15} {N,4} {K,2} {D,2} {W,1} {Count,2}  {Left(Setup.Mean,5)} ±{Right(Setup.Sd,5)}  {Left(Run.Mean,5)} ±{Right(Run.Sd,5)}  {Left(Total.Mean,5)} ±{Right(Total.Sd,5)}  {Left(Gap.Mean,5)} ±{Right(Gap.Sd,5)}";
        }

        private static string Left(double number, int totalWidth)
        {
            var max = Math.Pow(10, totalWidth) - 1;
            if (number > max) 
                number = max;
            
            string output = "";
            int decimalPlaces = totalWidth - 2; //because every decimal contains at least "0."
            bool isError = true;

            while (isError && decimalPlaces >= 0)
            {
                output = Math.Round(number, decimalPlaces).ToString(NumberFormatInfo.InvariantInfo);
                isError = output.Length > totalWidth;
                decimalPlaces--;
            }

            if (isError)
            {
                throw new FormatException($"{number} can't be represented in {totalWidth} characters");
            }
        
            return output.PadLeft(totalWidth);
        }

        private static string Right(double number, int totalWidth)
        {
            var max = Math.Pow(10, totalWidth) - 1;
            if (number > max) 
                number = max;

            string output = "";
            int decimalPlaces = totalWidth - 2; //because every decimal contains at least "0."
            bool isError = true;

            while (isError && decimalPlaces >= 0)
            {
                output = Math.Round(number, decimalPlaces).ToString(NumberFormatInfo.InvariantInfo);
                isError = output.Length > totalWidth;
                decimalPlaces--;
            }

            if (isError)
            {
                throw new FormatException($"{number} can't be represented in {totalWidth} characters");
            }
        
            return output.PadRight(totalWidth);
        }
    }
}
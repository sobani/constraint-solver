namespace Solver.Explorer;

public static class Extensions
{
    public static IEnumerable<TResult> GroupByAdjacent<TSource, TKey, TResult>(
        this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector,
        Func<TKey, IReadOnlyList<TSource>, TResult> resultSelector)
    {
        List<TSource> group = [];
        TKey? key = default;
        foreach (var item in source)
        {
            var currKey = keySelector(item);
            if (group is [])
            {
                key = currKey;
                group.Add(item);
                continue;
            }

            if (Equals(key, currKey))
            {
                group.Add(item);
                continue;
            }

            yield return resultSelector(key!, group);

            key = currKey;
            group = [item];
        }
        
        if (group is not [])
            yield return resultSelector(key!, group);
    }


    public static Stats Stats<T>(this IReadOnlyCollection<T> source, Func<T, double> selector)
    {
        var min = source.Min(selector);
        var max = source.Max(selector);
        var mean = source.Average(selector);
        var variance = source
            .Select(selector)
            .Average(value => (value - mean) * (value - mean));
        
        return new Stats(min, max, mean, variance);
    }
}

public record Stats(double Min, double Max, double Mean, double Var)
{
    public double Sd => Math.Sqrt(Var);
}